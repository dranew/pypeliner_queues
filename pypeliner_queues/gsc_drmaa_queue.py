import pypeliner.execqueue.drmaa


class GSCDrmaaJobQueue(pypeliner.execqueue.drmaa.DrmaaJobQueue):
    def create(self, ctx, name, sent, temps_dir):
        native_spec = self.native_spec
        if 'io' in ctx:
            native_spec += ' -l shah_io={}'.format(ctx['io'])
        return pypeliner.execqueue.drmaa.DrmaaJob(
            ctx, name, sent, temps_dir, self.modules, self.qenv, native_spec, self.session)

