from setuptools import setup, find_packages

setup(
    name='pypeliner_queues',
    packages=find_packages(),
    description='A collection of pypeliner queues for specific systems.',
    author='Andrew McPherson',
    author_email='andrew.mcpherson@gmail.com',
    url='http://bitbucket.org/dranew/pypeliner_queues',
)
